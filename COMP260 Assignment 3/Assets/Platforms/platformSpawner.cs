﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class platformSpawner : MonoBehaviour {


    public float platformSpeed;
    public GameObject platform;
    GameObject tempPlatform;
    public float platformSpaceing;
    private int platformNumber = 1;
    private float heightV;

    // Use this for initialization
    void Start() {
        InvokeRepeating("OutputTime", 0f, platformSpaceing);  
    }

    void OutputTime()
    {
        heightV = Random.Range(-1.6f, 0.6f);

        tempPlatform = Instantiate(platform, this.transform, false);

        tempPlatform.transform.parent = transform;

        Vector2 spawnH;

        spawnH.x = 0;
        spawnH.y = heightV;

        tempPlatform.transform.localPosition = spawnH;

        tempPlatform.gameObject.name = "platform " + platformNumber;

        platformNumber += 1;

    }

    void Update()
    {
        
    }
}
