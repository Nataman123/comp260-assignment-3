﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public float jumpHeight;
    public static bool canJump = true;
    public float xBound;
    public float yBound;

    private Vector2 startPoint;

	// Use this for initialization
	void Start () {
        startPoint = transform.position;
    }

    // Update is called once per frame
    void Update() {

        Vector2 jumping;
        jumping.y = jumpHeight;
        jumping.x = 0;

        if (Input.GetKeyDown(KeyCode.Space) && canJump == true){
            GetComponent<Rigidbody2D>().AddForce(jumping, ForceMode2D.Impulse);
            canJump = false;
        }

        if (GetComponent<Rigidbody2D>().velocity.y == 0) {
            canJump = true;
        }

        if (transform.position.x < -xBound ||transform.position.y < -yBound) {
            transform.position = startPoint;
        }
    }
}
