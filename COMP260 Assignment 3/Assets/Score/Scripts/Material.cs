﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Material : MonoBehaviour {

	//MOVEMENT
	public float vel;
	public float killX;

	public int material;

	



	


	// Use this for initialization
	void Start () {
		 
	}
	
	// Update is called once per frame
	void Update () {
		
        Vector2 velocity;
        velocity.x = -vel;
        velocity.y = 0;

        transform.Translate(velocity * Time.deltaTime);

        if (transform.position.x < -killX) {
            Destroy(this.gameObject);
        }
	}

	void OnTriggerEnter2D(Collider2D col){
		

		Scorekeeper.Instance.onScore(material);
		gameObject.SetActive(false);
	}
}
