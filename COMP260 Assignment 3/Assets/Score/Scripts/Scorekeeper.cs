﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
public class Scorekeeper : MonoBehaviour {

	static private Scorekeeper instance;

	static public Scorekeeper Instance {
		get {return instance;}
	}

	public int pointsPerHay = 5;
	private int pointsPerStick = 10;
	private int pointsPerBrick = 15;
	private int score = 0;

	public Text scoreText;

	public AudioClip hayClip;
	public AudioClip stickClip;
	public AudioClip brickClip;
    private AudioSource audioSrc;

	// Use this for initialization
	void Start () {
	audioSrc = GetComponent<AudioSource>();

		if(instance == null){
			instance = this;
		} else{
			Debug.LogError(
				"More than one Scorekeeper exsits in the scene");
		}

		scoreText.text = "0";
		
	}

	public void onScore(int material){
		
		switch(material)
		{
			case 1:
				audioSrc.PlayOneShot(hayClip);
				score += pointsPerHay;
				scoreText.text = score.ToString();
				break;
			case 2:
				audioSrc.PlayOneShot(stickClip);
				score += pointsPerStick;
				scoreText.text = score.ToString();
				break;
			case 3:
				audioSrc.PlayOneShot(brickClip);
				score += pointsPerBrick;
				scoreText.text = score.ToString();
				break;

		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
